package br.ucsal.testequalidade.calculoetui;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Assertions;

import br.ucsal.testequalidade.TuiUtil;

public class TuiUtilMock extends TuiUtil {

	// Map<mensagem, qtd>
	private Map<String, Integer> qtdChamadasExibirMensagem = new HashMap<>();

	@Override
	public Integer obterNumeroInteiroPositivo() {
		return 2;
	}

	@Override
	public void exibirMensagem(String mensagem) {
		if (!qtdChamadasExibirMensagem.containsKey(mensagem)) {
			qtdChamadasExibirMensagem.put(mensagem, 0);
		}
		qtdChamadasExibirMensagem.put(mensagem, qtdChamadasExibirMensagem.get(mensagem) + 1);
	}

	/**
	 * Verificar (assert) se o método exibirMensagem foi chamado uma única vez, com
	 * o parâmetro parametroDesejado.
	 * 
	 * @param parametroDesejado parâmetro necessário para verificação de chamada.
	 */
	public void verificarChamadaExibirMensagem(String parametroDesejado) {
		Assertions.assertEquals(1, qtdChamadasExibirMensagem.get(parametroDesejado));
	}
}
